This package includes `orphaner`, a text menu frontend to `deborphan`.

The files `orphaner`, `orphaner.8` and `editkeep.8` are copied from `deborphan`
version 1.7.33. The frontend used to be part of `deborphan` package, but then it
was removed in version 1.7.34.
